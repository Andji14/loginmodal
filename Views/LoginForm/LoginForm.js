import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    ListView,
    

} from 'react-native';
import Dimensions from 'Dimensions';
import List from './List';
import Login from './Login';


var { height, width } = Dimensions.get('window');

export default class LoginForm extends Component {
    static LoginForm = {
        title: 'LoginForm',
    }
    constructor(props) {
        super(props);

    }
    render() {
       
        return (

            <View style={styles.container}>


                <View style={styles.topNavigation}>
                    <TouchableOpacity onPress={() => { navigate('#') }} style={{ width: 20 }} >
                        <Image source={require('./img/arrow.png')} style={{ width: 20, height: 20 }} />
                    </TouchableOpacity >

                    <TouchableOpacity onPress={() => { navigate('#') }} style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 60, width: width - 20 }} >
                        <View>
                            <Text style={{ fontSize: 22, color: '#1a9e50', fontFamily: "Intro Inline" }}> АЗ-КМЕТЪТ </Text>
                        </View>

                        <View style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-end', backgroundColor: 'pink' }} >
                            <Image source={require('./img/bg-4-icons-single copy.png')} style={{ width: 130, height: 3, }} />
                        </View>
                    </TouchableOpacity >
                </View>
                <ScrollView>

                    <View style={styles.contCategImg} >
                        <View style={styles.contCategImgBox}>
                            <Image source={require('./img/flag.png')} style={{ width: 30, height: 30 }} />
                            <Text style={{ fontSize: 15, color: 'black', height: 45, textAlignVertical: 'center' }} > Статус: </Text>
                        </View>

                        <View style={styles.contCategImgBox}>
                            <Text style={{ fontSize: 15, color: 'black', height: 45, textAlignVertical: 'center' }} > В обработка </Text>
                        </View>

                        <View style={styles.contCategImgBox}>
                            <TouchableOpacity style={{ width: 40, height: 40, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
                                <Image source={require('./img/pencil-outline copy.png')} style={{ width: 30, height: 30 }} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: 40, height: 40, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
                                <Image source={require('./img/add-user.png')} style={{ width: 30, height: 30 }} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: 40, height: 40, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
                                <Image source={require('./img/dustbin-thin.png')} style={{ width: 30, height: 30 }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.contCategImg} >
                        <View style={styles.contCategImgBox2}>
                            <Image source={require('./img/placeholder.png')} style={{ width: 30, height: 30 }} />
                            <Text style={{ fontSize: 15, color: 'black', height: 45, textAlignVertical: 'center' }} > Адрес: </Text>
                        </View>

                        <View style={styles.contCategImgBox3}>
                            <Text style={{ fontSize: 15, color: 'black', height: 45, textAlignVertical: 'center' }} > ул. Петър Берон 8, Бургас </Text>
                        </View>

                    </View>

                    <View style={styles.addImgCont2}>
                        <Image source={require('./img/map-excerpt.png')} style={styles.imgBack2} />
                    </View>


                    <View style={styles.contCategImg} >
                        <View style={styles.contCategImgBox4}>
                            <Image source={require('./img/clock.png')} style={{ width: 30, height: 30 }} />
                            <Text style={{ fontSize: 15, color: '#1ba754', height: 45, textAlignVertical: 'center' }} > Докладвано на: </Text>
                        </View>

                        <View style={styles.contCategImgBox4}>
                            <Text style={{ fontSize: 15, color: 'black', height: 45, textAlignVertical: 'center' }} > 24. Януари 2017 </Text>
                        </View>
                    </View>



                    <View style={styles.addImgCont3}>
                        <Image source={require('./img/Pothole.png')} style={styles.imgBack3} />
                    </View>


                    <View style={styles.contCategImg} >
                        <View style={styles.contCategImgBox4}>
                            <Image source={require('./img/clock.png')} style={{ width: 30, height: 30 }} />
                            <Text style={{ fontSize: 15, color: '#1ba754', height: 45, textAlignVertical: 'center' }} > Подкрепено от: </Text>
                        </View>

                        <View style={styles.contCategImgBox4}>
                            <Text style={{ fontSize: 15, color: 'black', height: 45, textAlignVertical: 'center' }} > 48 жители </Text>
                        </View>
                    </View>





                    <View style={[{ flex: 1 }, styles.container]}>
                        <List />
                    </View>






                    <View style={styles.contCategImg} >
                        <View style={styles.contCategImgBox4}>
                            <Text style={{ fontSize: 15, color: '#1ba754', height: 45, textAlignVertical: 'center' }} > КОМЕНТАРИ </Text>
                        </View>
                        <View style={styles.contCategImgBox4}>
                            <Text></Text>
                        </View>
                    </View>






                    <View style={styles.contCategImg2} >
                        <View style={styles.contCategImgBox5}>
                            <Image source={require('./img/facebook-profile-pic.png')} style={{ width: 40, height: 40 }} />
                            <View style={styles.img} >
                                <Text style={{ fontSize: 10, color: 'black', textAlign: 'center' }} > Петър Иванов </Text>
                            </View>
                        </View>

                        <View style={styles.contCategImgBox6}>
                            <Text style={{ fontSize: 12, color: 'black', textAlignVertical: 'center', padding: 10, }} > Lorem ipsum dolor sit
                                amet, consectetur adipiscing elit. PoteraLorem ipsum dolor sit amet, consectetur adipiscing elit. Potera
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Potera
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Potera
                                 </Text>
                        </View>
                    </View>
                    <View style={styles.contCategImg2} >
                        <View style={styles.contCategImgBox6}>
                            <Text style={{ fontSize: 12, color: 'white', textAlignVertical: 'center', padding: 10, backgroundColor: '#1ba754' }} > Lorem ipsum dolor sit
                                amet, consectetur adipiscing elit. PoteraLorem ipsum dolor sit amet, consectetur adipiscing elit. Potera
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Potera
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Potera
                                 </Text>
                        </View>

                        <View style={styles.contCategImgBox8}>
                            <Image source={require('./img/facebook-profile-pic.png')} style={{ width: 40, height: 40 }} />
                            <View style={styles.img} >
                                <Text style={{ fontSize: 10, color: 'black', textAlign: 'center' }} > Петър Иванов </Text>
                            </View>
                            <View style={styles.icons}  >
                                <TouchableOpacity>
                                    <Image source={require('./img/pencil-outline.png')} style={{ width: 20, height: 20 }} />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image source={require('./img/delete-button.png')} style={{ width: 20, height: 20 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

      

                </ScrollView>
                
                <View style={styles.bottomIcons}>
                    <TouchableOpacity onPress={() => { navigate('#') }} style={styles.citizenContImg2} >
                        <Image source={require('./img/victory copy.png')} style={{ width: 35, height: 35 }} />
                        <Text style={{ fontSize: 12, color: 'black', height: 20, textAlignVertical: 'center' }} > Подкрепи  </Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { navigate('#') }} style={styles.citizenContImg2} >
                        <Image source={require('./img/fountain-pen copy.png')} style={{ width: 35, height: 35 }} />
                        <Text style={{ fontSize: 12, color: 'black', height: 20, textAlignVertical: 'center' }} > Коментирай </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { navigate('#') }} style={styles.citizenContImg2} >
                        <Image source={require('./img/facebook.png')} style={{ width: 35, height: 35 }} />
                        <Text style={{ fontSize: 12, color: 'black', height: 20, textAlignVertical: 'center' }} > Сподели в FB </Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => { navigate('#') }} style={styles.citizenContImg2} >
                        <Image source={require('./img/share.png')} style={{ width: 35, height: 35 }} />
                        <Text style={{ fontSize: 12, color: 'black', height: 20, textAlignVertical: 'center' }} > LOGIN </Text>
                    </TouchableOpacity>
                </View>


            </View>



        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    topNavigation: {
        width: width,
        height: 60,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        justifyContent: 'space-between',
        padding: 10,
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },

    contCategImg: {
        width: width,
        height: 40,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contCategImgBox: {
        width: (width / 3),
        height: 40,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contCategImgBox2: {
        height: 40,
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    contCategImgBox3: {
        height: 40,
        display: 'flex',
        flexGrow: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    addImgCont2: {
        width: width,
        height: 90,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imgBack2: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 90
    },
    contCategImgBox4: {
        width: (width / 2),
        height: 40,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    addImgCont2: {
        width: width,
        height: 30,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    imgBack2: {
        top: 0,
        left: 0,
        width: width,
        height: 30
    },
    citizens: {
        width: width,
        height: 60,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        justifyContent: 'center',
    },
    citizenContImg: {
        width: 70,
        height: 60,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },






    contCategImg2: {
        width: width,
        height: 110,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    contCategImgBox5: {
        height: 100,
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contCategImgBox6: {
        width: width - 100,
        height: 100,
        display: 'flex',
        flexGrow: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    img: {
        width: 40,
        height: 40,
        flexDirection: 'column',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contCategImgBox6: {
        width: 150,
        height: 100,
        display: 'flex',
        flexGrow: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contCategImgBox8: {
        height: 100,
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icons: {

        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bottomIcons: {
        width: width,
        height: 60,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        justifyContent: 'center',
    },
    citizenContImg2: {
        width: (width / 4),
        height: 60,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
});