import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    FlatList,
    Image,
    SeparatorComponent,
    ListView
} from 'react-native';

const show_first = [
    {
        key: 1,
        name: 'Петър',
        image: './img/facebook-profile-pic.png',

    },
    {
        key: 2,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 3,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 4,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 5,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 6,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 7,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 8,
        name: 'Петър',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
]


const show_second = [
    {
        key: 9,
        name: 'Amy Farha',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',

    },
    {
        key: 10,
        name: 'Chris Jackson',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 11,
        name: 'Amy Farha',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',

    },
    {
        key: 12,
        name: 'Chris Jackson',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 13,
        name: 'Amy Farha',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',

    },
    {
        key: 14,
        name: 'Chris Jackson',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
    {
        key: 15,
        name: 'Amy Farha',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',

    },
    {
        key: 16,
        name: 'Chris Jackson',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',

    },
]



//https://www.youtube.com/watch?v=dfF1TDAE49o

class List extends Component {

    _renderItem(item) {
        return (
            <View style={{paddingLeft:10, paddingRight:10}}>
                <Image style={{ width: 40, height: 40 }} source={require('./img/facebook-profile-pic.png')} />
                <Text style={{ fontSize: 10, color: 'black', height: 20, textAlign: 'center' }}> Иван </Text>
            </View>
        )
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View >

                    <FlatList
                        horizontal
                        SeparatorComponent={() => <View style={{ padding: 10, backgroundColor: 'red' }} />}
                        renderItem={({ item }) => this._renderItem(item)}
                        data={show_first}

                    />


                </View>
            </View>



        )
    }
}

const styles = StyleSheet.create({
    text: {
        color: 'black'
    },
    citizens: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        justifyContent: 'center',
    },
    citizenContImg: {
        width: 70,
        height: 60,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },

})

export default List



