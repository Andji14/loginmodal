import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  KeyboardAvoidingView,
  TouchableHighlight
} from 'react-native';
import Dimensions from 'Dimensions';
import Modal from 'react-native-modal'


var { height, width } = Dimensions.get('window');

export default class Login extends Component {

  state = {
    modalVisible: false,
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />

        <View style={styles.inputUserContainer}>
          <View style={styles.iconContainer}>
            <Image
              source={require('./img/username-icon.png')}
              style={styles.icon}
            />
          </View>

          <TextInput
            placeholder="username or email"
            placeholderTextColor="rgba(255,255,255,0.7)"
            returnKeyType="next"
            onSubmitEditing={() => this.passwordInput.focus()}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.input}
            placeholderTextColor="black"
          />
        </View>


        <View style={styles.inputUserContainer}>

          <View style={styles.iconContainer}>
            <Image
              source={require('./img/password-icon.png')}
              style={styles.icon}
            />
          </View>

          <TextInput

            placeholder="password"
            secureTextEntry
            returnKeyType="go"
            placeholderTextColor="black"
            ref={(input) => this.passwordInput = input}
            style={styles.input}

          />
        </View>


        <TouchableOpacity onPress={() => {
          this.setModalVisible(true)
        }}
          style={styles.buttonContainer}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>


        <Modal
          visible={this.state.modalVisible}
          animationType="slide"
          onRequestClose={() => { alert("Modal has been closed.") }}
        >
          <View style={{ width: width - 40, height: 50, backgroundColor: 'white', alignItems: 'center' }}>
            <Text>Hello!</Text>
            <TouchableHighlight onPress={() => {
              this.setModalVisible(!this.state.modalVisible)
            }}>
              <Text>Hide Modal</Text>
            </TouchableHighlight>
          </View>
        </Modal>


      </View>

    );
  }
}


const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  input: {
    height: 40,
    marginBottom: 10,
    color: 'black',
    paddingHorizontal: 10,
    flexGrow: 1,
    textDecorationLine: 'none',
    opacity: 0.5,

  },
  buttonContainer: {
    backgroundColor: '#2980b9',
    paddingVertical: 15,

  },
  buttonText: {
    textAlign: 'center',
    color: "#FFFFFF",
    fontWeight: '700',

  },
  inputUserContainer: {

    height: 60,
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',


  },

  iconContainer: {
    width: 60,
    height: 60,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

  },
  icon: {
    width: 20,
    height: 20
  },

});






















