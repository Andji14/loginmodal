import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    KeyboardAvoidingView
} from 'react-native';
import Dimensions from 'Dimensions';
import Login from './Login';

var { height, width } = Dimensions.get('window');

export default class LogoWithLogin extends Component {
    // static LogoWithLogin = {
    //     title: 'LogoWithLogin',
    // }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <KeyboardAvoidingView behaviour="position" style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('./img/background.jpg')}
                    />
                </View>

                <View style={styles.formContainer}>
                    <Login />
                </View>

            </KeyboardAvoidingView>
        );
    }
}
// ba77ff

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'pink',
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        width: 100,
        height: 100,
    },

});